import yaml
import requests
from requests.auth import HTTPBasicAuth

from bitbucket_pipes_toolkit import Pipe


schema = {
    'BITBUCKET_REPO_OWNER_UUID': {'required': True, 'type': 'string'},
    'BITBUCKET_REPO_SLUG': {'required': True, 'type': 'string'},
    'BITBUCKET_USERNAME': {'required': True, 'type': 'string'},
    'BITBUCKET_APP_PASSWORD': {'required': True, 'type': 'string'},
    'CACHES_COUNT': {'required': False, 'type': 'integer', 'default': 1},
    'CACHES': {'required': False, 'type': 'list', 'default': []}
}

with open('/usr/bin/pipe.yml', 'r') as metadata_file:
    metadata = yaml.safe_load(metadata_file.read())

pipe = Pipe(pipe_metadata=metadata, schema=schema, check_for_newer_version=True)


def run_pipe():
    pipe.log_info('Executing the pipe...')
    account_uuid = pipe.get_variable('BITBUCKET_REPO_OWNER_UUID')
    repo_uuid = pipe.get_variable('BITBUCKET_REPO_SLUG')
    bitbucket_user = pipe.get_variable('BITBUCKET_USERNAME')
    bitbucket_password = pipe.get_variable('BITBUCKET_APP_PASSWORD')

    caches_to_clear = pipe.get_variable('CACHES')

    url = 'https://api.bitbucket.org/internal/repositories/{' + account_uuid + '}/' + repo_uuid + '/pipelines_caches/?page=1&pagelen=100'
    auth = HTTPBasicAuth(bitbucket_user, bitbucket_password)

    response = requests.get(url, auth=auth)
    if not response.ok:
        pipe.fail(f'Failed to retrieve caches: {response.status_code} {response.json()} {response.request.url}')
    pipe.log_debug(response.request.headers)
    pipe.log_debug(response.content)
    response_json = response.json()

    if not caches_to_clear:
        clear_all_caches(account_uuid=account_uuid, repo_uuid=repo_uuid,
                         cache_list_json=response_json["values"], auth=auth)
    else:
        clear_selected_caches(account_uuid=account_uuid, repo_uuid=repo_uuid,
                              cache_list_json=response_json["values"], caches_to_clear=caches_to_clear, auth=auth)

    pipe.success('Finished clearing caches')


def clear_all_caches(account_uuid, repo_uuid, cache_list_json, auth):
    pipe.success('Retrieved {} caches'.format(len(cache_list_json)))
    if len(cache_list_json) == 0:
        pipe.log_warning('No caches were found!')
    for cache in cache_list_json:
        pipe.log_debug(cache)
        clear_cache_by_uuid(account_uuid=account_uuid, repo_uuid=repo_uuid,
                            cacheUuid=cache["uuid"], cache_name=cache["name"], auth=auth)


def clear_selected_caches(account_uuid, repo_uuid, cache_list_json, caches_to_clear, auth):
    for cache in cache_list_json:
        pipe.log_debug(cache)
        if cache["name"] in caches_to_clear:
            clear_cache_by_uuid(account_uuid=account_uuid, repo_uuid=repo_uuid,
                                cacheUuid=cache["uuid"], cache_name=cache["name"], auth=auth)


def clear_cache_by_uuid(account_uuid, repo_uuid, cacheUuid, cache_name, auth):
    delte_url = 'https://api.bitbucket.org/internal/repositories/{' + \
        account_uuid + '}/' + repo_uuid + '/pipelines_caches/' + cacheUuid
    delete_response = requests.delete(delte_url, auth=auth)

    if delete_response.ok:
        pipe.success('Successfully cleared cache {}'.format(cache_name))
    else:
        response_json = delete_response.json()
        pipe.fail(f"Failed to clear cache {cache_name}: {response_json}")


if __name__ == '__main__':
    run_pipe()
