# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 2.0.4

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.

## 2.0.3

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 2.0.2

- patch: Add warning message about new version of the pipe available.

## 2.0.1

- patch: Fixed missing docker image name in the pipe metadata file

## 2.0.0

- major: Pipe name was changed from clear-cache to bitbucket-clear-cache

## 1.0.1

- patch: Updated the documentation

## 1.0.0

- major: Initial public release of the pipe

## 0.1.3

- patch: Updated pipes toolkit version to fix coloring of log info messages.

## 0.1.2

- patch: Updated the internal library version

## 0.1.1

- patch: Improve documentation and release process

## 0.1.0

- minor: Initial release

